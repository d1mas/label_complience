Python scripts for 'verify-slave-label' Jenkins job.
======================

The repository used as temp. SVC for testing purpose

Content:

* labels.yaml - main YAML file with jobs and params

* generate_yaml.py - a script for parse XML configs of jobs
  and creation 'labels.yaml'

* parse_yaml.py* - core job script which make parsing
  and print params for 'verify-slave-label' job