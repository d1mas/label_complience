#!/usr/bin/env python
#
# usage example: ./generate_yaml.py ../../output/patching-ci/
# ../../output/patching-ci/ - path to generated xml configs

import os
import sys

from lxml import etree

def show_labels(tox_output_dir):

	tox_output_dir = os.path.abspath(tox_output_dir)
	tox_output_files = os.listdir(tox_output_dir)

	for filename in tox_output_files:

		filepath = os.path.join(tox_output_dir, filename)
		doc = etree.parse(filepath)
		label = doc.find ('assignedNode')

		if label is None:
			print 'NA' + ' ' + filename
		else:
			print label.text + ' ' + filename

show_labels(sys.argv[1])